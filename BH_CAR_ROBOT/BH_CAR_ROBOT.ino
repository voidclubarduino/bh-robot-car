#include <Servo.h> 
#include <NewPing.h>
#define TRIG_PIN A0
#define ECHO_PIN A1
#define HEAD_SERVO_PIN 8
char dist[3];
char rot[3];
int rotation = 0;
String output = "";
int IN1 = 4;
int IN2 = 5;
int IN3 = 6;
int IN4 = 7;
Servo headServo;
NewPing sonar(TRIG_PIN, ECHO_PIN, 400); 
void setup() {
  //SONAR
  pinMode (TRIG_PIN, OUTPUT);
  pinMode (ECHO_PIN, INPUT);
  //HEAD SERVO
  headServo.attach(HEAD_SERVO_PIN);
  //MOTORS
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  for (int deg = 30; deg < 140; deg+=5) {
    headServo.write(deg);
    delay(10);
    displaySonar(deg);
  }

  // scan left to right
  for (int deg = 140; deg > 30; deg-=5) {
    headServo.write(deg);
    delay(10);
   displaySonar(deg);
  }
  
  
}
  
void displaySonar(int degrees) {
  int distance = sonar.ping_cm(); 
  if (distance < 0) distance = 0; 
  Serial.println(distance);
  if(distance < 70 && distance != 0){
    long delayT = millis() + 2000;
    while(millis()< delayT){
    rear();
    }
  }else{
    front();
  }

}
void front(){
 digitalWrite(IN1, LOW);
 digitalWrite(IN2, HIGH);
 digitalWrite(IN3, HIGH);
 digitalWrite(IN4, LOW);
}
void rear(){
 digitalWrite(IN1, HIGH);
 digitalWrite(IN2, LOW);
 digitalWrite(IN3, LOW);
 digitalWrite(IN4, HIGH);
  
  } 
void stop(){
 //Para o motor A
 digitalWrite(IN1, HIGH);
 digitalWrite(IN2, HIGH);
  //Para o motor B
 digitalWrite(IN3, HIGH);
 digitalWrite(IN4, HIGH);
 Serial.println("PARA");
 }
